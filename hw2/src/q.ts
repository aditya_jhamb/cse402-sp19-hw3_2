/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        throw new Error("Call chaining not implemented.");
    }

    apply(callback: (datum: any) => any): ASTNode {
        throw new Error("Call chaining not implemented.");
    }

    count(): ASTNode {
        throw new Error("Call chaining not implemented.");
    }

    product(query: ASTNode): ASTNode {
        throw new Error("Call chaining not implemented.");
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        throw new Error("Call chaining not implemented.");
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    constructor(callback: (datum: any) => any) {
        super("Apply");
        throw new Error("Unimplemented AST node " + this.type);
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
        throw new Error("Unimplemented AST node " + this.type);
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...


//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        throw new Error("Unimplemented AST node " + this.type);
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        throw new Error("Unimplemented AST node " + this.type);
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    constructor(...args) { // you may want to add some proper arguments to this constructor
        super("Join");
        throw new Error("Unimplemented AST node " + this.type);
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    constructor(...args) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        throw new Error("Unimplemented AST node " + this.type);
    }
}
