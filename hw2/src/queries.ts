import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.IdNode();
export const autoTheftsQuery = new q.IdNode();

//// 1.4 clean the data

export const cleanupQuery = new q.IdNode();

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q;
export const theftsQuery2 = Q;
export const autoTheftsQuery2 = Q;

//// 4 put your queries here (remember to export them for use in tests)
