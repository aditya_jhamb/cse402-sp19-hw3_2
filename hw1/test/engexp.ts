import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("EngExp", () => {
    it("should handle invalid and duplicated flags", () => {
        function f(flags) {
            return new EngExp()
                .match("foo")
                .withFlags(flags)
                .asRegExp();
        }

        function e1() {
            return f("QWETuioytaosdf");
        }
        const e2 = f("iiim");

        expect(e1).to.throw(EngExpError);
        expect(e2.test("FoO")).to.be.true;
    });

    it("should parse a basic URL", () => {
        const e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(" ")
            .endOfLine()
            .asRegExp();
        expect(e.test("https://www.google.com/maps")).to.be.true;
        expect(e.test("https://www googlecom")).to.be.false;
        expect(e.test("http2://www.googlecom/foo")).to.be.false;
    });

    it("should parse a URL with query and fragment", () => {
        const separators = "?=& #";
        const keyArg = new EngExp()
            .anythingBut(separators)
            .then("=")
            .anythingBut(separators);
        const keyArg2 = new EngExp()
            .match("&")
            .then(keyArg);
        const query = new EngExp()
            .match("?")
            .then(keyArg)
            .zeroOrMore(keyArg2);
        const fragment = new EngExp()
            .match("#")
            .anythingBut(separators);

        const e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(separators)
            .maybe(query)
            .maybe(fragment)
            .endOfLine()
            .asRegExp();
        expect(e.test("https://www.google.com/maps")).to.be.true;
        expect(e.test("https://www.google.com/maps?foo=baz")).to.be.true;
        expect(e.test("https://www.google.com/maps?foo=bar&baz=quux&i=dum#pipe")).to.be.true;
        expect(e.test("https://www.google.com/maps#pipe")).to.be.true;
        expect(e.test("https://www.google.com/maps?foo=bar&baz=quux&i==dum#pipe")).to.be.false;
        expect(e.test("https://www.google.com/maps?foo=bar&baz=quux&i=dum#pi=pe")).to.be.false;
        expect(e.test("https://www.google.com/maps?x=y=z")).to.be.false;
    });

    it("match, then, zeroOrMore, oneOrMore, optional, and maybe should agree for string repeated exactly once", () => {
        const pref = "can ";
        const s = "the * escape?";
        const es = new EngExp().match(s);

        const e1 = new EngExp()
            .startOfLine()
            .match(pref + s)
            .endOfLine()
            .asRegExp();
        const e2 = new EngExp()
            .startOfLine()
            .then(pref)
            .then(s)
            .endOfLine()
            .asRegExp();
        const e3 = new EngExp()
            .startOfLine()
            .match(pref)
            .zeroOrMore(es)
            .endOfLine()
            .asRegExp();
        const e4 = new EngExp()
            .startOfLine()
            .match(pref)
            .oneOrMore(es)
            .endOfLine()
            .asRegExp();
        const e5 = new EngExp()
            .startOfLine()
            .match(pref)
            .then(es.optional())
            .endOfLine()
            .asRegExp();
        const e6 = new EngExp()
            .startOfLine()
            .match(pref)
            .maybe(s)
            .endOfLine()
            .asRegExp();

        const hmmm = pref + "the       escap";

        expect(e1.test(pref + s)).to.be.true;
        expect(e1.test(hmmm)).to.be.false;
        expect(e2.test(pref + s)).to.be.true;
        expect(e2.test(hmmm)).to.be.false;
        expect(e3.test(pref + s)).to.be.true;
        expect(e3.test(hmmm)).to.be.false;
        expect(e4.test(pref + s)).to.be.true;
        expect(e4.test(hmmm)).to.be.false;
        expect(e5.test(pref + s)).to.be.true;
        expect(e5.test(hmmm)).to.be.false;
        expect(e6.test(pref + s)).to.be.true;
        expect(e6.test(hmmm)).to.be.false;
    });

    it("should parse a disjunctive date pattern", () => {
        const month = new EngExp()
            .match("Jan").or("Feb").or("Mar").or("Apr")
            .or("May").or("Jun").or("Jul").or("Aug")
            .or("Sep").or("Oct").or("Nov").or("Dec");
        const e = new EngExp()
            .startOfLine()
            .then(
             new EngExp()
                .digit().repeated(1, 2)
                .then("/")
                .then(new EngExp().digit().repeated(1, 2))
                .then("/")
                .then(new EngExp().digit().repeated(2, 4))
                .or(
                 new EngExp()
                    .digit().repeated(1, 2)
                    .then(" ")
                    .then(month)
                    .then(" ")
                    .then(new EngExp().digit().repeated(2, 4))
                )
            )
            .endOfLine()
            .asRegExp();
        expect(e.test("12/25/2015")).to.be.true;
        expect(e.test("25 Dec 2015")).to.be.true;
        expect(e.test("11/11/11/11")).to.be.false;
        expect(e.test("111 Dec 9999")).to.be.false;
    });

    it("disjunctive date pattern with levels", () => {
        const month = new EngExp()
            .match("Jan").or("Feb").or("Mar").or("Apr")
            .or("May").or("Jun").or("Jul").or("Aug")
            .or("Sep").or("Oct").or("Nov").or("Dec");
        const e = new EngExp()
            .startOfLine().beginLevel()
            .digit().repeated(1, 2)
            .then("/")
            .then(new EngExp().digit().repeated(1, 2))
            .then("/")
            .then(new EngExp().digit().repeated(2, 4))
            .or(
             new EngExp()
                .digit().repeated(1, 2)
                .then(" ")
                .then(month)
                .then(" ")
                .then(new EngExp().digit().repeated(2, 4))
            )
            .endLevel().endOfLine()
            .asRegExp();
        expect(e.test("12/25/2015")).to.be.true;
        expect(e.test("25 Dec 2015")).to.be.true;
        expect(e.test("11/11/11/11")).to.be.false;
        expect(e.test("111 Dec 9999")).to.be.false;
    });

    it("should capture nested groups", () => {
        const e = new EngExp()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
              .beginCapture()
                .anythingBut("/")
              .endCapture()
              .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        const result = e.exec("https://www.google.com/maps");
        expect(result[1]).to.be.equal("google.com/maps");
        expect(result[2]).to.be.equal("google.com");
    });

    it("should capture nested groups and work with disjunctions", () => {
        const e = new EngExp()
            .startOfLine().beginLevel()
            .then("http")
            .or("https")
            .then("://")
            .maybe("www.")
            .beginCapture()
              .beginCapture()
                .anythingBut("/")
              .endCapture()
              .anythingBut(" ")
            .endCapture()
            .endLevel().endOfLine()
            .asRegExp();
        const result = e.exec("https://www.google.com/maps");
        expect(result[1]).to.be.equal("google.com/maps");
        expect(result[2]).to.be.equal("google.com");
    });

    // ---- EXTRA CREDIT: named groups ----

    /*
    // Once you complete the extra credit, you can uncomment these tests,
    // -- otherwise tsc will complain.

    it("asRegExp returns a NamedRegExp instead of a normal RegExp", () => {
        console.log("  extra credit -- these tests are optional");

        const e = new EngExp().match("foo").asRegExp();

        expect(e.exec("foo")).to.have.property("groups");
    });

    it("extra credit example from handout", () => {
        function captureMonth(name) {
            return new EngExp()
                .beginCapture(name)
                .then("Jan").or("Feb").or("Mar").or("Apr")
                .or("May").or("Jun").or("Jul").or("Aug")
                .or("Sep").or("Oct").or("Nov").or("Dec")
                .endCapture();
        }

        function captureNumber(name, minDigits, maxDigits) {
            return new EngExp()
                .beginCapture(name)
                .digit().repeated(minDigits, maxDigits)
                .endCapture();
        }

        function date1(suffix = "1") {
            return new EngExp()
                .then(captureNumber("mm" + suffix, 1, 2))
                .then("/")
                .then(captureNumber("dd" + suffix, 1, 2))
                .then("/")
                .then(captureNumber("yy" + suffix, 2, 4));
        }

        function date2(suffix = "2") {
            return new EngExp()
                .then(captureNumber("dd" + suffix, 1, 2))
                .then(" ")
                .then(captureMonth("mm" + suffix))
                .then(" ")
                .then(captureNumber("yy" + suffix, 2, 4));
        }

        const e = new EngExp()
            .startOfLine().beginLevel()
            .then(date1())
            .or(date2())
            .endLevel().endOfLine()
            .asRegExp();

        const result1 = e.exec("8/26/2017");
        expect(result1.groups.get("dd1")).to.be.equal("26");
        expect(result1.groups.get("mm1")).to.be.equal("8");
        expect(result1.groups.get("yy1")).to.be.equal("2017");

        const result2 = e.exec("26 Aug 2017");
        expect(result2.groups.get("dd2")).to.be.equal("26");
        expect(result2.groups.get("mm2")).to.be.equal("Aug");
        expect(result2.groups.get("yy2")).to.be.equal("2017");

        // don't fail if you don't match - can be tricky
        const result3 = e.exec("66 666 66666");
        expect(result3).to.be.null;
    });
    */
});
