const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this.expressionStack = [null];
        this._usedHelpers = {};
        this.lastHelperName = [];
        this.blockNameStack = [];
        this.registerBlockHelper("each", function (ctx, body, list) {
            var result = "";
            for(var x in list) {
                result += body(list[x]);
            }
            return result;
        });

        this.registerBlockHelper("if", function (ctx, body, expression) {
            if(expression) {
                return body(ctx);
            } else {
                return "";
            }
        });
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._usedHelpers = {};
        this._bodyStack = [];
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        for(var localFunction in this._helpers.expr) {
            if (!this._usedHelpers.hasOwnProperty(localFunction)) {
                continue;
            }
            this._bodyStack[this._bodyStack.length - 1] = `var __$${localFunction} = ${this._helpers.expr[localFunction].toString()}\n` + 
                this._bodyStack[this._bodyStack.length - 1];
        }

        for(var blockFunction in this._helpers.block) {
            this._bodyStack[this._bodyStack.length - 1] = `var __$${blockFunction} = ${this._helpers.block[blockFunction].toString()}\n` + 
                this._bodyStack[this._bodyStack.length - 1];
        }
        return this.popScope();
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        this._bodyStack[this._bodyStack.length - 1] += `return ${this._outputVar};\n`;
        return new Function(this._inputVar, this._bodyStack.pop());
    }

    append(expr) {
        this._bodyStack[this._bodyStack.length - 1] += `${this._outputVar} += ${expr};\n`;
    }

    exitINT(ctx) {
        var s = ctx.getText();
        if (s.includes("0x") || s.includes("0X")) {
            var re = /(0x|0X)/g
            var arr = s.split(re);
            s = arr[0] + arr[2]
            this.expressionStack.push(parseInt(s, 16));
        } else if (s.includes("0b") || s.includes("0B")) {
            var re = /(0b|0B)/g
            var arr = s.split(re);
            s = arr[0] + arr[2]
            this.expressionStack.push(parseInt(s, 2));
        } else if (s.includes("0o") || s.includes("0O")) {
            var re = /(0o|0O)/g
            var arr = s.split(re);
            s = arr[0] + arr[2]
            this.expressionStack.push(parseInt(s, 8));
        } else if (s.startsWith("-0") || s.startsWith("0")) {
            this.expressionStack.push(parseInt(s, 8));
        } else {
            this.expressionStack.push(parseInt(s, 10));
        }
    }

    exitSTRING(ctx) {
        this.expressionStack.push(`"${escapeString(ctx.getText())}"`);
    }

    exitFLOAT(ctx) {
        this.expressionStack.push(parseFloat(ctx.getText()));
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitDataLookup(ctx) {
        this.expressionStack.push(this._inputVar + "." + ctx.getText());
    }

    exitExpressionElement(ctx) {
        this.append(this.expressionStack.pop());
    }

    exitHelper(ctx) {
        this._usedHelpers[ctx.getText()] = true;
        this.lastHelperName.push(ctx.getText());
    }

    exitHelperFunction(ctx) {
        var result = "__$" + this.lastHelperName.pop() + "(" + this._inputVar;
        var arglist = "";
        while(this.expressionStack[this.expressionStack.length - 1] != null) {
            var arg = this.expressionStack.pop()
            if (typeof arg === "string" || arg instanceof String) {
                // Get rid of formatting that args dont need.
                arg = arg.replace(/\\/g, '').replace(/\'/g, '');
            }
            arglist = ", " + arg + arglist;
        }
        result += arglist + ")"
        this.expressionStack.push(result);
    }

    exitStartBlock(ctx) {
        this.pushScope();
        // save our arg list for later.
        this.expressionStack.push(null);
        // Give our exit expression element nothing to append;
        //this.expressionStack.push("");
    }

    exitEndBlock(ctx) {
        var closingBlock = this.blockNameStack.pop();
        var openingBlock = this.blockNameStack.pop()
        if(closingBlock !== openingBlock) {
            throw "Block start '" + openingBlock + "' does not match the block end '" + closingBlock + "'.";
        }

        var arglist = ""; 
        // Get rid of null we added earlier
        this.expressionStack.pop();
        while(this.expressionStack[this.expressionStack.length - 1] != null) {
            var arg = this.expressionStack.pop()
            if (typeof arg === "string" || arg instanceof String) {
                // Get rid of formatting that args dont need.
                arg = arg.replace(/\\/g, '').replace(/\'/g, '');
            }
            arglist = ", " + arg + arglist;
        }

        var result = "__$" + openingBlock + "(" + this._inputVar + ", " + this.popScope().toString() + arglist + ")";
        
        this.append(result);
        this.expressionStack.push(result);
    }

    exitBlockName(ctx) {
        this.blockNameStack.push(ctx.getText());
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
