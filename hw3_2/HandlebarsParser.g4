parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement  
    : TEXT
    | BRACE TEXT
    ;

commentElement : START COMMENT END_COMMENT ;

expressionElement
    : START bracketedElement END
    ;

bracketedElement
    : dataLookup # Lookup
    | literal # Consta
    | OPEN_PAREN bracketedElement CLOSE_PAREN # Paren
    | helper bracketedElement+ # HelperFunction
    ;

literal
    : INTEGER   # INT
    | FLOAT     # FLOAT
    | STRING    # STRING
    ;

dataLookup
    : ID
    ;

helper 
    : ID
    ;

blockElement
    : START BLOCK blockName bracketedElement* END # StartBlock
    | START CLOSE_BLOCK blockName END # EndBlock
    ;

blockName
    : ID
    ;