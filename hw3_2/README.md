This directory contains a starter kit for Homework 3.2, "a Templating
Language with ANTLR4".

The compiler comes in three parts: an ANTLR4 grammar split across
HandlebarsLexer.g4 and HandlebarsParser.g4, and a listener-based
compiler in src/compiler.js. ANTLR4 will know what to do to combine
the separate Lexer and Parser grammars for your language.

An ANTLR4 runtime is provided for you in the antlr/ directory. As long as you
have a relatively recent JRE available on your system, you should be able to
use it seamlessly from npm. grun.sh and grun.bat should also work the same as
they did for Homework 3.1. To view a graphical parse tree for Example 1 from the
handout, you can run:

```
$ ./grun.sh Handlebars document -gui < examples/ex1.html
```

To compiler your grammar, run `npm run antlr4`. As with Homeworks 1 and 2,
we will be testing using chai and mocha; you can run the provided tests with
`npm test`. You can also compile template files directly (or use them as
tests) with the provided driver.js. 

For example,

```
$ node driver.js examples/ex1.html
<html><body>Hello world  and hello again</body></html>


```

will run the compiler on the file examples/ex1.html and dump the compiled
and rendered output to stdout. You can also specify a context (as a
JavaScript source file to include) with the -c option:

```
$ node driver.js examples/ex1.html -c examples/ex1.js
```

If you pass the -t option, then the driver will run a test. It takes
a context and an expected output file passed with -e, or if none are provided,
tries to infer them based on the name of the input file

```
$ node driver.js examples/ex1.html -t
Running test: Example 1

compiled examples/ex1.html to function with source:
===================================================================
function anonymous(_$ctx
/*``*/) {
var _$result = "";
_$result += "<html><body>Hello world ";
_$result += " and hello again</body></html>\n";
return _$result;

}

applying to context object read from examples/ex1.js:
===================================================================
{}

rendered output text:
===================================================================
<html><body>Hello world  and hello again</body></html>


TEST PASS
```

The driver also exports an interface that can be used to help automate
tests that depend on external files - you can see how this works in
test/custom.js.
