lexer grammar HandlebarsLexer;

// Default mode: everything outside of a construct
TEXT : ~[{]+ ;
BRACE : '{' ;
START : '{{' -> mode(Island) ;

mode Island;

BLOCK : '#' ;
CLOSE_BLOCK : '/';
ID : [a-zA-Z_] [a-zA-Z0-9_]* ;
COMMENT : '!--'  -> mode(Comment);
WS : [ \t\n\r]+ -> channel(HIDDEN) ;
END : '}}' -> mode(DEFAULT_MODE) ;
OPEN_PAREN : '(' ;
CLOSE_PAREN : ')' ;

// Lexing number literals is hard. The order of these rules and their
// cases is important; we need to be careful to avoid, for example,
// parsing '0.1' as (INTEGER 0) (FLOAT .1) or as (FLOAT 1.) (Integer 0).

fragment PM      : [+\-]              ;
fragment INT_NLZ : '0' | [1-9] [0-9]* ; // no leading zeros
fragment INT_LZ  : [0-9]+             ; // can have leading zeroes
fragment EXP     : [Ee] PM? INT_LZ    ;

FLOAT :   PM? INT_NLZ? '.'  INT_LZ EXP? 
    | PM? INT_NLZ? '.' EXP?
    | PM? INT_LZ EXP
    ;

fragment HEX : [0-9a-fA-F] ;
fragment OCT : [0-7] ;
fragment BIN : [01] ;

INTEGER
    : PM? INT_NLZ       // decimal integer
    | PM? '0' OCT+      // octal integer (specified by leading zeros)
    | PM? '0' [Xx] HEX+ // hexadecimal integer
    | PM? '0' [Oo] OCT+ // octal integer
    | PM? '0' [Bb] BIN+ // binary integer
    ;

fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment ESC     :   '\\' (['\\/bfnrt] | UNICODE) ;

STRING :  '\'' (ESC | ~['\\])* '\'' ;

mode Comment ;

ANY : .+? -> channel(HIDDEN) ;
END_COMMENT : '--}}' -> mode(DEFAULT_MODE) ;
